package com.libfsm;

import com.libfsm.automata.machines.DFA;
import com.libfsm.operations.common.OperationType;
import com.libfsm.operations.binary.BinaryOperationFactory;

import java.util.Set;

public class Main {
    public static void main(String[] args) {

        /*nfa.setAlphabet(Set.of('0','1'));
        nfa.addState("1");
        nfa.addState("2");
        nfa.addTransition("1", '0', "2");
        nfa.addTransition("2", '1', "2");
        nfa.setInitial("1");
        nfa.addStateFinal("2");
         */

        var first = new DFA("asf");
        first.setAlphabet(Set.of('a'));
        first.addState("1");
        first.addStateFinal("1");
        first.setInitial("1");
        first.addTransition("1", 'a', "1");

        var second = new DFA("asd");
        second.setAlphabet(Set.of('a', 'b'));
        second.addState("2");
        second.addStateFinal("2");
        second.setInitial("2");
        second.addTransition("2", 'a', "2");
        second.addTransition("2", 'b', "2");

        System.out.println(new BinaryOperationFactory().get(OperationType.Intersection).apply(first, second));


        //var nfa = NFAEx01.get();
        //System.out.println(nfa);
        //System.out.println("*****************");
        //System.out.println(new NFANormalFormFactory().get(NormalFormType.EpsilonFree).apply(nfa));
        //System.out.println(dfa.compute("baa", false).getSecond());
    }
}
