package com.libfsm.examples.dfa;

import com.libfsm.automata.machines.DFA;
import com.libfsm.automata.machines.FSM;

import java.util.Set;

public class DFAEx01 {
    public static FSM get() {
        var machine = new DFA("#a=2(mod 3)");
        machine.addState("$");
        machine.addState("a");
        machine.addState("aa");
        machine.addStateFinal("aa");
        machine.setAlphabet(Set.of('a', 'b'));
        machine.addTransition("$", 'b', "$");
        machine.addTransition("$", 'a', "a");
        machine.addTransition("a", 'b', "a");
        machine.addTransition("a", 'a', "aa");
        machine.addTransition("aa", 'b', "aa");
        machine.addTransition("aa", 'a', "$");
        machine.setInitial("$");
        return machine;
    }
}
