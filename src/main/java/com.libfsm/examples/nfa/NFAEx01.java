package com.libfsm.examples.nfa;

import com.libfsm.automata.machines.FSM;
import com.libfsm.automata.machines.NFA;

import java.util.Set;

public class NFAEx01 {
    public static FSM get() {
        var machine = new NFA("endsWith('a')");
        machine.addState("$");
        machine.addState("a");
        machine.addStateFinal("a");
        machine.addState("trash");
        machine.setAlphabet(Set.of('a','b'));
        machine.addTransition("$", 'a', "$");
        machine.addTransition("$", 'a', "a");
        machine.addTransition("$", 'b', "$");
        machine.addTransition("$", 'b', "trash");
        machine.addTransition("a", 'a', "a");
        machine.setInitial("$");
        return machine;
    }
}
