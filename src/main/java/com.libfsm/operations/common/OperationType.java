package com.libfsm.operations.common;

public enum OperationType {
    Complement, Concatenation, Intersection, Reverse, Union
}
