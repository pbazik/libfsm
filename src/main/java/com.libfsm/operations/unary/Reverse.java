package com.libfsm.operations.unary;

import com.libfsm.automata.machines.FSM;
import com.libfsm.automata.machines.NFA;
import com.libfsm.normalforms.common.NormalFormType;
import com.libfsm.normalforms.common.NormalFormFactory;

import java.util.Map;

class Reverse implements UnaryOperation {
    private Map<Integer, String> steps = Map.ofEntries(
            Map.entry(0, "0. In this step we convert input machine to its piggy normal form"),
            Map.entry(1, "1. In this step we copy alphabet from input machine"),
            Map.entry(2, "2. In this step we copy states from input machine"),
            Map.entry(3, "3. In this step we copy and reverse transitions from input machine"),
            Map.entry(4, "4. In this step we set final state of input machine as initial state of result machine"),
            Map.entry(5, "5. In this step we set initial state of input machine as final state of result machine"),
            Map.entry(6, "Completed")
    );

    @Override
    public FSM apply(FSM fsm) {
        var result = new NFA("Reverse ".concat(fsm.getName()));

        var piggyNormalForm = new NormalFormFactory()
                .get(NormalFormType.Piggy)
                .apply(fsm);

        result.setAlphabet(piggyNormalForm.getAlphabet());

        piggyNormalForm.getStates().forEach(result::addState);

        piggyNormalForm.getStates().forEach(source ->
                piggyNormalForm.getStates().forEach(destination ->
                        piggyNormalForm.getAllTransitionsFromTo(source, destination).forEach(c ->
                                result.addTransition(destination, c, source)
                        )
                )
        );

        var oldFinal = piggyNormalForm.getFinalStates().iterator().next();
        result.setInitial(oldFinal);

        var oldInitial = piggyNormalForm.getInitial();
        result.addStateFinal(oldInitial);

        return result;
    }

    @Override
    public Map<Integer, String> getSteps() {
        return steps;
    }
}
