package com.libfsm.operations.unary;

import com.libfsm.automata.machines.FSM;

import java.util.Map;

public interface UnaryOperation {
    FSM apply(FSM fsm);
    Map<Integer, String> getSteps();
}
