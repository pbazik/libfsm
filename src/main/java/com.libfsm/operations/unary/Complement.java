package com.libfsm.operations.unary;

import com.libfsm.automata.helpers.Sets;
import com.libfsm.automata.machines.DFA;
import com.libfsm.automata.machines.FSM;

import java.util.Map;

class Complement implements UnaryOperation {
    private Map<Integer, String> steps = Map.ofEntries(
            Map.entry(0, "0. No special init step\n"),
            Map.entry(1, "1. In this step we copy alphabet from input dfa"),
            Map.entry(2, "2. In this step we copy states from input dfa"),
            Map.entry(3, "3. In this step we copy transitions from input dfa"),
            Map.entry(4, "4. In this step we set initial state of input dfa as initial state of result dfa"),
            Map.entry(5, "5. In this step we set rejecting states of input dfa as final states of result dfa"),
            Map.entry(6, "Completed")
    );

    @Override
    public FSM apply(FSM fsm) {
        var result = new DFA("Complement ".concat(fsm.getName()));

        result.setAlphabet(fsm.getAlphabet());

        var dfaStates = fsm.getStates();
        dfaStates.forEach(result::addState);

        fsm.getStates().forEach(p ->
            fsm.getStates().forEach(q ->
                fsm.getAllTransitionsFromTo(p, q).forEach(c ->
                        result.addTransition(p, c, q)
                )
            )
        );

        result.setInitial(fsm.getInitial());

        var dfaFinalStates = fsm.getFinalStates();
        var resultFinalStates = Sets.difference(dfaStates, dfaFinalStates);
        resultFinalStates.forEach(result::addStateFinal);

        return result;
    }

    @Override
    public Map<Integer, String> getSteps() {
        return steps;
    }
}
