package com.libfsm.operations.unary;


import com.libfsm.operations.common.OperationType;

public class UnaryOperationFactory {
    public UnaryOperation get(OperationType type) {
        switch (type) {
            case Complement:
                return new Complement();
            case Reverse:
                return new Reverse();
            default:
                throw new UnsupportedOperationException();
        }
    }
}
