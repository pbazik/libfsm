package com.libfsm.operations.binary;

import com.libfsm.automata.helpers.Pair;
import com.libfsm.automata.helpers.Sets;
import com.libfsm.automata.machines.DFA;
import com.libfsm.automata.machines.FSM;

import java.util.Map;
import java.util.Set;

class Intersection implements BinaryOperation {
    private Map<Integer, String> steps = Map.ofEntries(
            Map.entry(0, "0. No special init step\n"),
            Map.entry(1, "1. In this step we set result's alphabet as intersection of alphabets of input automata"),
            Map.entry(2, "2. In this step we add cartesian product states into result"),
            Map.entry(3, "3. In this step we add transitions to result automaton in following way:\n" +
                    "delta([q_x, q_y], c) <- [delta_first(q_x, c), delta_second(q_y, c)]"),
            Map.entry(4, "4. In this step we set state [first_initial, second_initial] as initial state of result"),
            Map.entry(5, "5. In this step we set final states in following way:\n" +
                    "if q_x is final in first automaton and q_y is final in second automaton," +
                    " then [q_x, q_y] is final in result"),
            Map.entry(6, "Completed")
    );

    public FSM apply(FSM first, FSM second) {
        var result = new DFA(first.getName().concat(" intersection ").concat(second.getName()));

        var alphabet = Sets.intersection(first.getAlphabet(), second.getAlphabet());
        result.setAlphabet(alphabet);

        var product = Sets.cartesianProduct(first.getStates(), second.getStates());
        product.forEach(pair -> result.addState(pair.toString()));

        product.forEach(pair -> {
            alphabet.forEach(c -> {
                result.addTransition(
                        pair.toString(),
                        c,
                        new Pair<>(first.computeStep(Set.of(pair.getFirst()), c).iterator().next(),
                                second.computeStep(Set.of(pair.getSecond()), c).iterator().next())
                                .toString()
                );
            });

            if (first.isFinalState(pair.getFirst()) && second.isFinalState(pair.getSecond()))
                result.addStateFinal(pair.toString());
        });

        result.setInitial(new Pair<>(first.getInitial(), second.getInitial()).toString());

        return result;
    }

    @Override
    public Map<Integer, String> getSteps() {
        return steps;
    }
}
