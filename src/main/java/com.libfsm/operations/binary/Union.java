package com.libfsm.operations.binary;

import com.libfsm.automata.helpers.Sets;
import com.libfsm.automata.machines.FSM;
import com.libfsm.automata.machines.NFA;

import java.util.Map;

class Union implements BinaryOperation {
    private Map<Integer, String> steps = Map.ofEntries(
            Map.entry(0, "0. No special init step\n"),
            Map.entry(1, "1. In this step we set result's alphabet as union of alphabets of input automata"),
            Map.entry(2, "2. In this step we copy states from first input automata"),
            Map.entry(3, "3. In this step we copy states from second input automata"),
            Map.entry(4, "4. In this step we add new state to result automaton, it will be initial state. In this " +
                    "state result automaton can \"guess\" which input automaton accepts input word. Therefore it will contain " +
                    "epsilon transitions pointing to initial states of input automata"),
            Map.entry(5, "5. In this step we copy transitions from first input automata"),
            Map.entry(6, "6. In this step we copy transitions from second input automata"),
            Map.entry(7, "7. In this step we add transitions described in step 4."),
            Map.entry(8, "8. In this step we set new <init> state as initial state of result"),
            Map.entry(9, "9. In this step we set final states of first automaton as final states of result"),
            Map.entry(10, "10. In this step we set final states of second automaton as final states of result"),
            Map.entry(11, "Completed")
    );

    public FSM apply(FSM first, FSM second) {
        var result = new NFA(first.getName().concat(" union ").concat(second.getName()));

        var alphabet = Sets.union(first.getAlphabet(), second.getAlphabet());
        result.setAlphabet(alphabet);

        var unionStates = Sets.union(first.getStates(), second.getStates());
        var init = "<init ".concat(result.getName()).concat(">");
        unionStates.add(init);
        unionStates.forEach(result::addState);

        first.getStates().forEach(source ->
                first.getStates().forEach(destination ->
                        first.getAllTransitionsFromTo(source, destination).forEach(c ->
                                result.addTransition(source, c, destination)
                        )
                )
        );

        second.getStates().forEach(source ->
                second.getStates().forEach(destination ->
                        second.getAllTransitionsFromTo(source, destination).forEach(c ->
                                result.addTransition(source, c, destination)
                        )
                )
        );
        result.addTransition(init, FSM.EPSILON, first.getInitial());
        result.addTransition(init, FSM.EPSILON, second.getInitial());

        result.setInitial(init);

        first.getFinalStates().forEach(result::addStateFinal);
        second.getFinalStates().forEach(result::addStateFinal);

        return result;
    }

    @Override
    public Map<Integer, String> getSteps() {
        return steps;
    }
}
