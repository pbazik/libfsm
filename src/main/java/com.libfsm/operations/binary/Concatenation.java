package com.libfsm.operations.binary;

import com.libfsm.automata.helpers.Sets;
import com.libfsm.automata.machines.FSM;
import com.libfsm.automata.machines.NFA;
import com.libfsm.normalforms.common.NormalFormType;
import com.libfsm.normalforms.common.NormalFormFactory;

import java.util.Map;

class Concatenation implements BinaryOperation {
    private Map<Integer, String> steps = Map.ofEntries(
            Map.entry(0, "0. At first, we transform both input automata into their piggy normal forms\n"),
            Map.entry(1, "1. In this step we set result's alphabet as union of alphabets of input automata"),
            Map.entry(2, "2. In this step we add first's states into result"),
            Map.entry(3, "3. In this step we add second's states into result"),
            Map.entry(4, "4. In this step we copy first's transitions to result"),
            Map.entry(5, "5. In this step we copy second's transitions to result"),
            Map.entry(6, "6. In this step we add transition from first's final to second's initial"),
            Map.entry(7, "7. In this step we set first's initial state as initial state of result"),
            Map.entry(8, "8. In this step we set final state of second automaton as final state of result"),
            Map.entry(9, "Completed")
    );

    public FSM apply(FSM first, FSM second) {
        var result = new NFA(first.getName().concat(" concat ").concat(second.getName()));

        var firstPiggy = new NormalFormFactory()
                .get(NormalFormType.Piggy)
                .apply(first);
        var secondPiggy = new NormalFormFactory()
                .get(NormalFormType.Piggy)
                .apply(second);

        var alphabet = Sets.union(firstPiggy.getAlphabet(), secondPiggy.getAlphabet());
        result.setAlphabet(alphabet);

        var concatStates = Sets.union(firstPiggy.getStates(), secondPiggy.getStates());
        concatStates.forEach(result::addState);

        firstPiggy.getStates().forEach(source ->
                firstPiggy.getStates().forEach(destination ->
                        firstPiggy.getAllTransitionsFromTo(source, destination).forEach(c ->
                                result.addTransition(source, c, destination)
                        )
                )
        );

        secondPiggy.getStates().forEach(source ->
                secondPiggy.getStates().forEach(destination ->
                        secondPiggy.getAllTransitionsFromTo(source, destination).forEach(c ->
                                result.addTransition(source, c, destination)
                        )
                )
        );
        var firstFinal = firstPiggy.getFinalStates().iterator().next();
        result.addTransition(firstFinal, FSM.EPSILON, secondPiggy.getInitial());

        result.setInitial(firstPiggy.getInitial());

        secondPiggy.getFinalStates().forEach(result::addStateFinal);

        return result;
    }

    @Override
    public Map<Integer, String> getSteps() {
        return steps;
    }
}
