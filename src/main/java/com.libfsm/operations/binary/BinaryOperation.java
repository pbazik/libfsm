package com.libfsm.operations.binary;


import com.libfsm.automata.machines.FSM;

import java.util.Map;

public interface BinaryOperation {
    FSM apply(FSM first, FSM second);
    Map<Integer, String> getSteps();
}
