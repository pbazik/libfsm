package com.libfsm.operations.binary;

import com.libfsm.operations.common.OperationType;

public class BinaryOperationFactory {
    public BinaryOperation get(OperationType type) {
        switch (type) {
            case Union:
                return new Union();
            case Intersection:
                return new Intersection();
            case Concatenation:
                return new Concatenation();
            default:
                throw new UnsupportedOperationException();
        }
    }
}
