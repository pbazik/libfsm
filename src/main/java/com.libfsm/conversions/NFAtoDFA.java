package com.libfsm.conversions;

import com.libfsm.automata.helpers.Sets;
import com.libfsm.automata.machines.DFA;
import com.libfsm.automata.machines.FSM;
import com.libfsm.normalforms.common.NormalFormType;
import com.libfsm.normalforms.common.NormalFormFactory;
import com.libfsm.operations.unary.UnaryOperation;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class NFAtoDFA implements UnaryOperation {
    private Map<Integer, String> steps = Map.ofEntries(
            Map.entry(0, "0. In this step we make from input nfa its equivalent epsilonfree normal form"),
            Map.entry(1, "1. In this step we copy alphabet from input nfa to resulting dfa"),
            Map.entry(2, "2. In this step we add powerset states into resulting dfa"),
            Map.entry(3, "3. In this step we add transitions to resulting dfa in following way:\n" +
                    "deltaResult(Q, a) = Union{q in Q}[deltaEpsilonfree(q, a)]"),
            Map.entry(4, "4. In this step we set a set containing initial state of input as" +
                    " initial state of resulting dfa"),
            Map.entry(5, "5. In this step we set final states of resulting dfa in such way that if" +
                    "any state q of input was final in input, then subset forming" +
                    " state in resulting dfa if contains q, then this state is final"),
            Map.entry(6, "Completed")
    );

    public FSM apply(FSM nfa) {
        FSM dfa = new DFA("DFAfromNFA");

        nfa = new NormalFormFactory().get(NormalFormType.EpsilonFree).apply(nfa);

        dfa.setAlphabet(nfa.getAlphabet());

        Sets.powerSet(nfa.getStates()).forEach(subset -> dfa.addState(stateFromSubset(subset)));

        for (var subset : Sets.powerSet(nfa.getStates())) {
            var source = stateFromSubset(subset);

            for (var symbol : nfa.getAlphabet()) {

                var destinations = new HashSet<>(nfa.computeStep(subset, symbol));
                var destination = stateFromSubset(destinations);

                dfa.addTransition(source, symbol, destination);

            }

            if (subset.stream().anyMatch(nfa::isFinalState))
                dfa.addStateFinal(source);
        }

        dfa.setInitial(stateFromSubset(Set.of(nfa.getInitial())));

        return dfa;
    }

    @Override
    public Map<Integer, String> getSteps() {
        return steps;
    }


    private static String stateFromSubset(Set<String> subset) {
        var result = new StringBuilder();
        result.append("{");
        result.append(String.join(", ", subset));
        result.append("}");
        return result.toString();
    }
}
