package com.libfsm.normalforms.nfa;

import com.libfsm.automata.machines.FSM;
import com.libfsm.automata.machines.NFA;
import com.libfsm.normalforms.common.NormalFormType;
import com.libfsm.normalforms.common.NormalFormFactory;
import com.libfsm.operations.unary.UnaryOperation;

import java.util.Map;

public class Piggy implements UnaryOperation {
    private Map<Integer, String> steps = Map.ofEntries(
            Map.entry(0, "0. In this step we create epsilon-free normal form of input nfa"),
            Map.entry(1, "1. In this step we copy alphabet from input nfa"),
            Map.entry(2, "2. In this step we copy states from input nfa"),
            Map.entry(3, "3. In this step we add new initial and final states to resulting piggy normal form"),
            Map.entry(4, "4. In this step we copy transitions from input nfa"),
            Map.entry(5, "5. In this step we add new transitions, one on epsilon from new initial do old initial" +
                    " and then from each old final state on epsilon to new final state"),
            Map.entry(6, "6. In this step we set new initial state as initial state"),
            Map.entry(7, "7. We add new final state as the only final state to resulting piggy"),
            Map.entry(8, "Completed")
    );

    @Override
    public FSM apply(FSM fsm) {
        var result = new NFA(fsm.getName());

        var epsilonFree = new NormalFormFactory()
                .get(NormalFormType.EpsilonFree)
                .apply(fsm);


        result.setAlphabet(epsilonFree.getAlphabet());

        epsilonFree.getStates().forEach(result::addState);

        var resultInit = "<init ".concat(fsm.getName()).concat(">");
        var resultFinal = "<final ".concat(fsm.getName()).concat(">");

        result.addState(resultInit);
        result.addState(resultFinal);

        epsilonFree.getStates().forEach(source ->
                epsilonFree.getStates().forEach(destination ->
                        epsilonFree.getAllTransitionsFromTo(source, destination).forEach(c ->
                                result.addTransition(source, c, destination)
                        )
                )
        );
        result.addTransition(resultInit, FSM.EPSILON, epsilonFree.getInitial());
        epsilonFree.getFinalStates().forEach(state ->
                result.addTransition(state, FSM.EPSILON, resultFinal)
        );

        result.setInitial(resultInit);

        result.addStateFinal(resultFinal);
        return result;
    }

    @Override
    public Map<Integer, String> getSteps() {
        return steps;
    }
}
