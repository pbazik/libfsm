package com.libfsm.normalforms.nfa;

import com.libfsm.automata.machines.FSM;
import com.libfsm.automata.machines.NFA;
import com.libfsm.operations.unary.UnaryOperation;

import java.util.Map;
import java.util.Set;

public class EpsilonFree implements UnaryOperation {
    private Map<Integer, String> steps = Map.ofEntries(
            Map.entry(0, "0. No special init step\n"),
            Map.entry(1, "1. In this step we copy alphabet from input nfa to resulting epsilon-free"),
            Map.entry(2, "2. In this step we copy states from input nfa"),
            Map.entry(3, "3. In this step we add transitions to resulting epsilon-free nfa in following way:\n" +
                    "Choose two states p, q connected by only epsilon transitions (from p to q), then any outgoing non-epsilon transition" +
                    " from q (to r) is added also as an outgoing transition from p (to r)."),
            Map.entry(4, "4. In this step we copy non-epsilon transitions from input nfa"),
            Map.entry(5, "5. In this step we copy initial state of input nfa as initial state of epsilon-free"),
            Map.entry(6, "6. In this step we copy final states from input to epsilon-free"),
            Map.entry(7, "7. For each state q, if epsilon closure of q in input nfa contains final state, we" +
                    " set q in epsilon-free as final"),
            Map.entry(8, "Completed")
    );

    @Override
    public FSM apply(FSM fsm) {
        var copyFSM = fsm.copy();
        var result = new NFA(copyFSM.getName());

        result.setAlphabet(copyFSM.getAlphabet());

        copyFSM.getStates().forEach(result::addState);

        copyFSM.getStates().forEach(q -> {
            var eClosure = copyFSM.epsilonClosure(Set.of(q));
            eClosure.forEach(eq -> {
                if (!eq.equals(q)) {
                    copyFSM.getAllTransitionsFrom(eq).forEach(pair -> {
                        result.addTransition(q, pair.getFirst(), pair.getSecond());
                    });
                }
            });
        });

        copyFSM.getTransitions().forEach((k, v) -> {
            v.forEach(q -> {
                if (!k.getSecond().equals(FSM.EPSILON))
                    result.addTransition(k.getFirst(), k.getSecond(), q);
            });
        });

        result.setInitial(copyFSM.getInitial());

        copyFSM.getFinalStates().forEach(result::addStateFinal);

        copyFSM.getStates().forEach(q -> {
            if (copyFSM.epsilonClosure(Set.of(q)).stream().anyMatch(copyFSM::isFinalState))
                result.addStateFinal(q);
        });

        return result;
    }

    @Override
    public Map<Integer, String> getSteps() {
        return steps;
    }
}
