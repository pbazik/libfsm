package com.libfsm.normalforms.common;

import com.libfsm.automata.helpers.Sets;
import com.libfsm.automata.machines.FSM;
import com.libfsm.operations.unary.UnaryOperation;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class ReachableStates implements UnaryOperation {
    private Map<Integer, String> steps = Map.ofEntries(
            Map.entry(0, "0. No special init step\n"),
            Map.entry(1, "1. In this step we compute unreachable states"),
            Map.entry(2, "2. In this step we remove unreachable states"),
            Map.entry(3, "Completed")
    );

    @Override
    public FSM apply(FSM fsm) {
        var result = fsm.copy();

        var unreachableStates = getUnreachableStates(fsm);

        unreachableStates.stream()
                .filter(result::isFinalState)
                .forEach(result::removeStateFinal);

        unreachableStates.forEach(result::removeState);

        return result;
    }

    public Set<String> getUnreachableStates(FSM fsm) {
        Set<String> reachableStates = new HashSet<>();
        reachableStates.add(fsm.getInitial());

        Set<String> newStates = new HashSet<>();
        newStates.add(fsm.getInitial());

        do {
            Set<String> temp = new HashSet<>();
            for (String state : newStates) {
                for (Character c : fsm.getAlphabet()) {
                    temp = Sets.union(temp, fsm.computeStep(Set.of(state), c));
                }
            }
            newStates = Sets.difference(temp, reachableStates);
            reachableStates = Sets.union(reachableStates, newStates);
        } while (!newStates.isEmpty());

        return Sets.difference(fsm.getStates(), reachableStates);
    }

    @Override
    public Map<Integer, String> getSteps() {
        return steps;
    }


}
