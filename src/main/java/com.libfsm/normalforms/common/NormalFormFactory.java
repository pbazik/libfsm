package com.libfsm.normalforms.common;

import com.libfsm.normalforms.nfa.EpsilonFree;
import com.libfsm.normalforms.nfa.Piggy;
import com.libfsm.operations.unary.UnaryOperation;

public class NormalFormFactory {
    public UnaryOperation get(NormalFormType type) {
        switch (type) {
            case Reachable:
                return new ReachableStates();
            case EpsilonFree:
                return new EpsilonFree();
            case Piggy:
                return new Piggy();
            default:
                throw new UnsupportedOperationException();
        }
    }
}
