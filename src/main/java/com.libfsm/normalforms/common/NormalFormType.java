package com.libfsm.normalforms.common;

public enum NormalFormType {
    Reachable, EpsilonFree, Piggy
}
