package com.libfsm.automata.helpers;

import java.util.HashSet;
import java.util.Set;

public class Sets {
    public static <T> Set<T> union(Set<T> a, Set<T> b) {
        Set<T> union = new HashSet<>(a);
        union.addAll(b);
        return union;
    }

    public static <T> Set<T> intersection(Set<T> a, Set<T> b) {
        Set<T> intersection = new HashSet<>(a);
        intersection.retainAll(b);
        return intersection;
    }

    public static <T> Set<T> difference(Set<T> a, Set<T> b) {
        Set<T> intersection = new HashSet<>(a);
        intersection.removeAll(b);
        return intersection;
    }

    public static <S, T> Set<Pair<S, T>> cartesianProduct(Set<S> a, Set<T> b) {
        Set<Pair<S, T>> product = new HashSet<>();

        a.forEach(s -> b.stream().map(t -> new Pair<>(s, t))
                        .forEach(product::add));

        return product;
    }

    public static <T> Set<Set<T>> powerSet(Set<T> originalSet) {
        Set<T> original = new HashSet<>(originalSet);
        Set<Set<T>> powerSet = new HashSet<>();
        var powerSetSize = 1 << original.size();

        for (int i = 0; i < powerSetSize; i++) {
            Set<T> subset = new HashSet<>();
            int mask = 1;

            for (T t : original) {
                if ((mask & i) > 0)
                    subset.add(t);
                mask <<= 1;
            }

            powerSet.add(subset);
        }

        return powerSet;
    }
}
