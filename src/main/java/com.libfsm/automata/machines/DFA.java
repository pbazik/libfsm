package com.libfsm.automata.machines;

import com.libfsm.automata.helpers.Pair;
import com.libfsm.automata.helpers.Triple;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.*;
import java.util.stream.Collectors;

public class DFA extends FSM {
    private Map<Pair<String, Character>, String> transitions = new HashMap<>();

    public DFA(String name) {
        super(name);
        fsmType = FSMType.DFA;
    }

    @Override
    public void addState(String state) {
        states.add(state);
    }

    @Override
    public void removeState(String state) {
        states.remove(state);

        var toRemove = new HashSet<Triple<String, Character, String>>();
        transitions.forEach((key, value) -> {
            if (key.getFirst().equals(state) || value.equals(state)) {
                toRemove.add(new Triple<>(key.getFirst(), key.getSecond(), value));
            }
        });

        toRemove.forEach(t -> removeTransition(t.getFirst(), t.getSecond(), t.getThird()));
    }

    @Override
    public void renameState(String oldName, String newName) {
        if (!states.contains(oldName) || states.contains(newName))
            return;

        var oldNameTransitions = new HashSet<Triple<String, Character, String>>();
        transitions.forEach((key, value) -> {
            if (key.getFirst().equals(oldName) || value.equals(oldName))
                oldNameTransitions.add(new Triple<>(key.getFirst(), key.getSecond(), value));
        });

        var oldNameFinal = isFinalState(oldName);
        var oldNameInitial = initial.equals(oldName);

        if (oldNameInitial)
            initial = newName;

        removeState(oldName);
        addState(newName);

        if (oldNameFinal) {
            removeStateFinal(oldName);
            addStateFinal(newName);
        }

        oldNameTransitions.forEach(t -> {
            removeTransition(t.getFirst(), t.getSecond(), t.getThird());

            if (t.getFirst().equals(oldName) && t.getThird().equals(oldName))
                addTransition(newName, t.getSecond(), newName);
            else if (t.getFirst().equals(oldName))
                addTransition(newName, t.getSecond(), t.getThird());
            else if (t.getThird().equals(oldName))
                addTransition(t.getFirst(), t.getSecond(), newName);
        });
    }

    @Override
    public void addTransition(String source, Character symbol, String destination) {
        if (states.contains(source)
                && states.contains(destination)
                && (alphabet.contains(symbol)))
            transitions.put(new Pair<>(source, symbol), destination);
    }

    @Override
    public void removeTransition(String source, Character symbol, String destination) {
        if (states.contains(source)
                && states.contains(destination)
                && (alphabet.contains(symbol)))
            transitions.remove(new Pair<>(source, symbol), destination);
    }

    @Override
    public Set<Character> getAllTransitionsFromTo(String source, String destination) {
        var result = new HashSet<Character>();
        transitions.forEach((k, v) -> {
            if (k.getFirst().equals(source) && v.equals(destination))
                result.add(k.getSecond());
        });
        return result;
    }

    @Override
    public Map<Pair<String, Character>, Set<String>> getTransitions() {
        var result  = new HashMap<Pair<String, Character>, Set<String>>();
        transitions.forEach((k, v) -> {
            result.put(k, Set.of(v));
        });
        return result;
    }

    @Override
    public Set<Pair<Character, String>> getAllTransitionsFrom(String source) {
        return null;
    }

    @Override
    public Map<Pair<String, Character>, Set<String>> getEpsilonTransitions() {
        return new HashMap<>();
    }

    @Override
    public void setAlphabet(Set<Character> alphabet) {
        this.alphabet.addAll(alphabet);

        var toRemove = new HashSet<Pair<String, Character>>();

        transitions.forEach((key, value) -> {
            if (!alphabet.contains(key.getSecond())) {
                toRemove.add(new Pair<>(key.getFirst(), key.getSecond()));
            }
        });

        toRemove.forEach(t -> transitions.remove(t));
    }

    @Override
    public Triple<Set<String>, Boolean, Integer> compute(String word, boolean verbose) {
        var currentStates = Set.of(initial);

        if (word.equals(EPSILON.toString()))
            return new Triple<>(currentStates,
                    currentStates.stream()
                            .allMatch(this::isFinalState),
                    word.length());

        for (var i = 0; i < word.length(); i++) {
            currentStates = computeStep(currentStates, word.charAt(i));

            if (currentStates.isEmpty())
                return new Triple<>(new HashSet<>(), false, i + 1);
        }

        return new Triple<>(currentStates,
                currentStates.stream()
                        .allMatch(this::isFinalState),
                word.length());
    }

    @Override
    public Set<String> computeStep(Set<String> states, Character c) {
        if (states.isEmpty() || !alphabet.contains(c))
            return new HashSet<>();
        return Set.of(transitions.get(new Pair<>(states.iterator().next(), c)));
    }

    @Override
    public Set<String> epsilonClosure(Set<String> states) {
        if (states.isEmpty())
            return new HashSet<>();
        return Set.of(states.iterator().next());
    }

    @Override
    public boolean isCorrectlyDefined() {
        if (initial.isEmpty() || alphabet.isEmpty() || states.isEmpty())
            return false;

        for (var q : states) {
            for (var c : alphabet) {
                if (!transitions.containsKey(new Pair<>(q, c)))
                    return false;
            }
        }

        return true;
    }

    @Override
    public String notCorrectlyDefinedText() {
        return "DFA is not defined correctly! Please check alphabet/initial state/transition function.";
    }

    @Override
    public void writeToFile(File file) {
        try (var printWriter = new PrintWriter(file)) {
            printWriter.print(this);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public FSM copy() {
        var result = new DFA(name);
        states.forEach(result::addState);
        finalStates.forEach(result::addStateFinal);
        result.setAlphabet(alphabet);
        result.setInitial(initial);
        transitions.forEach((k, v) ->
            result.addTransition(k.getFirst(), k.getSecond(), v)
        );
        return result;
    }

    @Override
    public String toString() {
        var sb = new StringBuilder();
        sb.append("{").append("\n");
        sb.append("\t");
        sb.append("\"type\" : \"DFA\",").append("\n");

        var alphFormat = String.join("\', \'",
                alphabet.stream()
                        .map(Object::toString)
                        .collect(Collectors.toSet()));
        sb.append("\t");
        sb.append("\"alphabet\" : [\'");
        sb.append(alphFormat).append("\'],").append("\n");

        var statesFormat = String.join("\", \"", states);
        sb.append("\t");
        sb.append("\"states\" : [\"");
        sb.append(statesFormat).append("\"],").append("\n");

        var transitionsFormat = new HashSet<String>();

        transitions.forEach((key, value) -> {
            transitionsFormat.add("\t\t{\n\t\t\t\"source\" : \""+key.getFirst()+"\",\n" +
                    "\t\t\t\"symbol\" : \'"+key.getSecond()+"\',\n" +
                    "\t\t\t\"destination\" : \""+value+"\"\n\t\t}");
        });

        var transitionsFormatAll = String.join(",\n", transitionsFormat);
        sb.append("\t");
        sb.append("\"transitions\" : [").append("\n");
        sb.append(transitionsFormatAll).append("\n\t],");

        sb.append("\n\t\"initial\" : \"");
        sb.append(initial);
        sb.append("\",\n");

        sb.append("\t\"final_states\" : [\"");
        var finalFormat = String.join("\", \"", finalStates);
        sb.append(finalFormat).append("\"]\n");
        sb.append("}");

        return sb.toString();
    }
}
