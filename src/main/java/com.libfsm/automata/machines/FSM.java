package com.libfsm.automata.machines;

import com.google.gson.*;
import com.libfsm.automata.helpers.Pair;
import com.libfsm.automata.helpers.Sets;
import com.libfsm.automata.helpers.Triple;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;

public abstract class FSM {
    public static final Character EPSILON = '$';
    protected FSMType fsmType;
    protected Set<Character> alphabet = new HashSet<>();
    protected Set<String> states = new HashSet<>();
    protected Set<String> finalStates = new HashSet<>();
    protected String initial = "";
    protected String name;

    public FSM(String name) {
        this.name = name;
    }

    public abstract void addState(String state);

    public abstract void removeState(String state);

    public abstract void renameState(String oldName, String newName);

    public abstract void addTransition(String source, Character symbol, String destination);

    public abstract void removeTransition(String source, Character symbol, String destination);

    public abstract Set<Character> getAllTransitionsFromTo(String source, String destination);

    public abstract Triple<Set<String>, Boolean, Integer> compute(String word, boolean verbose);

    public abstract Set<String> computeStep(Set<String> states, Character c);

    public abstract Set<String> epsilonClosure(Set<String> states);

    public abstract boolean isCorrectlyDefined();

    public abstract void setAlphabet(Set<Character> alphabet);

    public abstract void writeToFile(File file);

    public abstract String notCorrectlyDefinedText();

    public abstract Map<Pair<String, Character>, Set<String>> getEpsilonTransitions();

    public abstract Set<Pair<Character, String>> getAllTransitionsFrom(String source);

    public void addStateFinal(String state) {
        finalStates.add(state);
    }

    public void removeStateFinal(String state) {
        finalStates.remove(state);
    }

    public void setFsmType(FSMType fsmType) {
        this.fsmType = fsmType;
    }

    public FSMType getFsmType() {
        return fsmType;
    }

    public abstract Map<Pair<String, Character>, Set<String>> getTransitions();

    public Set<String> getStates() {
        return states;
    }

    public Set<String> getFinalStates() {
        return finalStates;
    }

    public boolean isFinalState(String state) {
        return finalStates.contains(state);
    }

    public Set<Character> getAlphabet() {
        return alphabet;
    }

    public String getInitial() {
        return initial;
    }

    public void setInitial(String state) {
        initial = state;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static boolean disjointStates(FSM first, FSM second) {
        return Sets.intersection(first.getStates(), second.getStates()).isEmpty();
    }

    public abstract FSM copy();

    public static FSM readFromFile(File file) {
        var parser = new JsonParser();
        JsonElement rootElement;
        FSM machine = new NFA("imported");

        try (var reader = new FileReader(file)) {
            rootElement = parser.parse(reader);
            var rootObject = rootElement.getAsJsonObject();

            var typePrimitive = rootObject.getAsJsonPrimitive("type");
            var type = typePrimitive.getAsString();

            var alphArray = rootObject.getAsJsonArray("alphabet");
            var alph = new HashSet<Character>();
            alphArray.forEach(e -> alph.add(e.getAsCharacter()));

            var statesArray = rootObject.getAsJsonArray("states");
            var states = new HashSet<String>();
            statesArray.forEach(e -> states.add(e.getAsString()));

            var transitionsArray = rootObject.getAsJsonArray("transitions");
            var transitions = new HashSet<Triple<String, Character, String>>();
            transitionsArray.forEach(e -> {
                var obj = e.getAsJsonObject();
                transitions.add(new Triple<>(
                        obj.getAsJsonPrimitive("source").getAsString(),
                        obj.getAsJsonPrimitive("symbol").getAsCharacter(),
                        obj.getAsJsonPrimitive("destination").getAsString()
                ));
            });

            var initialPrimitive = rootObject.getAsJsonPrimitive("initial");
            var initialState = initialPrimitive.getAsString();

            var finalStatesArray = rootObject.getAsJsonArray("final_states");
            var finalStates = new HashSet<String>();
            finalStatesArray.forEach(e -> finalStates.add(e.getAsString()));

            if (type.equals("DFA"))
                machine = new DFA("importedDFA");
            else
                machine = new NFA("importedNFA");

            machine.setAlphabet(alph);

            states.forEach(machine::addState);

            for (var t : transitions)
                machine.addTransition(t.getFirst(), t.getSecond(), t.getThird());

            machine.setInitial(initialState);

            finalStates.forEach(machine::addStateFinal);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return machine;
    }
}
