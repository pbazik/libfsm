package com.libfsm.automata.machines;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import com.libfsm.automata.helpers.Pair;
import com.libfsm.automata.helpers.Sets;
import com.libfsm.automata.helpers.Triple;

public class NFA extends FSM {
    private Map<Pair<String, Character>, Set<String>> transitions = new HashMap<>();

    public NFA(String name) {
        super(name);
        fsmType = FSMType.NFA;
    }

    @Override
    public void addState(String state) {
        states.add(state);
        alphabet.forEach(c -> transitions.put(new Pair<>(state, c), new HashSet<>()));
        transitions.put(new Pair<>(state, EPSILON), new HashSet<>());
    }

    @Override
    public void removeState(String state) {
        var toRemove = new HashSet<Triple<String, Character, String>>();

        transitions.forEach((key, value) -> {
            value.forEach(q -> {
                if (key.getFirst().equals(state) || q.equals(state)) {
                    toRemove.add(new Triple<>(key.getFirst(), key.getSecond(), q));
                }
            });
        });

        toRemove.forEach(t -> removeTransition(t.getFirst(), t.getSecond(), t.getThird()));
        states.remove(state);
    }

    @Override
    public void renameState(String oldName, String newName) {
        if (!states.contains(oldName) || states.contains(newName))
            return;

        var oldNameTransitions = new HashSet<Triple<String, Character, String>>();
        var toRemove = new HashSet<Pair<String, Character>>();

        transitions.forEach((key, value) -> {
            if (key.getFirst().equals(oldName))
                toRemove.add(new Pair<>(key.getFirst(), key.getSecond()));
            value.forEach(q -> {
                if (key.getFirst().equals(oldName) || q.equals(oldName))
                    oldNameTransitions.add(new Triple<>(key.getFirst(), key.getSecond(), q));
            });
        });

        var oldNameFinal = isFinalState(oldName);
        var oldNameInitial = initial.equals(oldName);

        if (oldNameInitial)
            initial = newName;

        removeState(oldName);
        addState(newName);

        if (oldNameFinal) {
            removeStateFinal(oldName);
            addStateFinal(newName);
        }

        oldNameTransitions.forEach(t -> {
            removeTransition(t.getFirst(), t.getSecond(), t.getThird());
            if (t.getFirst().equals(oldName) && t.getThird().equals(oldName))
                addTransition(newName, t.getSecond(), newName);
            else if (t.getFirst().equals(oldName))
                addTransition(newName, t.getSecond(), t.getThird());
            else if (t.getThird().equals(oldName))
                addTransition(t.getFirst(), t.getSecond(), newName);
        });

        toRemove.forEach(p -> transitions.remove(p));
    }

    @Override
    public void addTransition(String source, Character symbol, String destination) {
        if (source.equals(destination) && symbol.equals(EPSILON))
            return;

        if (states.contains(source)
                && states.contains(destination)
                && (alphabet.contains(symbol) || symbol.equals(EPSILON)))
            transitions.get(new Pair<>(source, symbol)).add(destination);
    }

    @Override
    public void removeTransition(String source, Character symbol, String destination) {
        if (states.contains(source)
                && states.contains(destination)
                && (alphabet.contains(symbol) || symbol.equals(EPSILON)))
            transitions.get(new Pair<>(source, symbol)).remove(destination);
    }

    @Override
    public Set<Character> getAllTransitionsFromTo(String source, String destination) {
        var result = new HashSet<Character>();
        transitions.forEach((k, v) -> {
            v.forEach(q -> {
                if (k.getFirst().equals(source) && q.equals(destination))
                    result.add(k.getSecond());
            });
        });
        return result;
    }

    @Override
    public Set<Pair<Character, String>> getAllTransitionsFrom(String source) {
        var result = new HashSet<Pair<Character, String>>();
        transitions.forEach((k, v) -> {
            v.forEach(dest -> {
                if (k.getFirst().equals(source) && !k.getSecond().equals(FSM.EPSILON))
                    result.add(new Pair<>(k.getSecond(), dest));
            });
        });
        return result;
    }

    @Override
    public Map<Pair<String, Character>, Set<String>> getTransitions() {
        return transitions;
    }

    @Override
    public Map<Pair<String, Character>, Set<String>> getEpsilonTransitions() {
        var result = new HashMap<Pair<String, Character>, Set<String>>();

        transitions.forEach((k, v) -> {
            if (k.getSecond().equals(FSM.EPSILON))
                result.put(k, v);
        });

        return result;
    }

    @Override
    public void setAlphabet(Set<Character> alphabet) {
        var oldAlphabet = this.alphabet;
        var intersection = Sets.intersection(alphabet, oldAlphabet);
        var diff = Sets.difference(alphabet, oldAlphabet);

        var remaining = new HashSet<Triple<String, Character, String>>();
        var newTransitions = new HashSet<Pair<String, Character>>();

        transitions.forEach((k, v) -> {
            if (v.isEmpty() && (intersection.contains(k.getSecond()) || k.getSecond().equals(EPSILON)))
                newTransitions.add(new Pair<>(k.getFirst(), k.getSecond()));
            v.forEach(q -> {
                if (intersection.contains(k.getSecond()) || k.getSecond().equals(EPSILON)) {
                    newTransitions.add(new Pair<>(k.getFirst(), k.getSecond()));
                    remaining.add(new Triple<>(k.getFirst(), k.getSecond(), q));
                }
            });
        });

        states.forEach(q ->
            diff.forEach(c -> {
                newTransitions.add(new Pair<>(q, c));
                newTransitions.add(new Pair<>(q, EPSILON));
            })
        );

        transitions.clear();
        newTransitions.forEach(p -> transitions.put(p, new HashSet<>()));
        remaining.forEach(t -> addTransition(t.getFirst(), t.getSecond(), t.getThird()));
        this.alphabet.addAll(alphabet);
    }

    @Override
    public Triple<Set<String>, Boolean, Integer> compute(String word, boolean verbose) {
        var currentStates = Set.of(initial);
        currentStates = epsilonClosure(currentStates);

        if (word.equals(EPSILON.toString()))
            return new Triple<>(currentStates,
                    currentStates.stream().anyMatch(this::isFinalState),
                    word.length());

        for (var i = 0; i < word.length(); i++) {
            currentStates = computeStep(currentStates, word.charAt(i));

            if (currentStates.isEmpty())
                return new Triple<>(currentStates, false, i + 1);
        }

        return new Triple<>(currentStates,
                currentStates.stream().anyMatch(this::isFinalState),
                word.length());
    }

    @Override
    public Set<String> computeStep(Set<String> states, Character c) {
        if (states.isEmpty() || !alphabet.contains(c))
            return new HashSet<>();

        //states.addAll(epsilonClosure(states));

        var resultStates = states.stream()
                .flatMap(q -> transitions.get(new Pair<>(q, c)).stream())
                .collect(Collectors.toSet());

        resultStates.addAll(epsilonClosure(resultStates));
        return resultStates;
    }

    @Override
    public Set<String> epsilonClosure(Set<String> states) {
        var closureStates = new HashSet<>(states);
        states.forEach(q -> epsilonClosureHelper(q, closureStates));
        return closureStates;
    }

    private void epsilonClosureHelper(String state, Set<String> closureStates) {
        transitions.get(new Pair<>(state, EPSILON))
                .forEach(q -> {
                    if (!closureStates.contains(q)) {
                        closureStates.add(q);
                        epsilonClosureHelper(q, closureStates);
                    }
                });
    }

    public static NFA fromDFA(DFA dfa) {
        var result = new NFA("NFAfromDFA");

        result.setAlphabet(dfa.getAlphabet());

        dfa.getStates().forEach(result::addState);

        dfa.getStates().forEach(p ->
                dfa.getStates().forEach(q ->
                        dfa.getAllTransitionsFromTo(p, q).forEach(c ->
                                result.addTransition(p, c, q)
                        )
                )
        );

        result.setInitial(dfa.getInitial());

        dfa.getFinalStates().forEach(result::addStateFinal);

        return result;
    }

    @Override
    public boolean isCorrectlyDefined() {
        return !initial.isEmpty() && !alphabet.isEmpty() && !states.isEmpty();
    }

    @Override
    public String notCorrectlyDefinedText() {
        return "NFA is not defined correctly! Please check alphabet/initial state.";
    }

    @Override
    public void writeToFile(File file) {
        try (var printWriter = new PrintWriter(file)) {
            printWriter.print(this);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public FSM copy() {
        var result = new NFA(name);
        states.forEach(result::addState);
        finalStates.forEach(result::addStateFinal);
        result.setAlphabet(alphabet);
        result.setInitial(initial);
        transitions.forEach((k, v) ->
                v.forEach(q ->
                        result.addTransition(k.getFirst(), k.getSecond(), q)
                )
        );
        return result;
    }

    @Override
    public String toString() {
        var sb = new StringBuilder();
        sb.append("{").append("\n");
        sb.append("\t");
        sb.append("\"type\" : \"NFA\",").append("\n");

        var alphFormat = String.join("\', \'",
                alphabet.stream()
                        .map(Object::toString)
                        .collect(Collectors.toSet()));
        sb.append("\t");
        sb.append("\"alphabet\" : [\'");
        sb.append(alphFormat).append("\'],").append("\n");

        var statesFormat = String.join("\", \"", states);
        sb.append("\t");
        sb.append("\"states\" : [\"");
        sb.append(statesFormat).append("\"],").append("\n");

        var transitionsFormat = new HashSet<String>();

        transitions.forEach((key, value) -> {
            value.forEach(q -> {
                transitionsFormat.add("\t\t{\n\t\t\t\"source\" : \""+key.getFirst()+"\",\n" +
                        "\t\t\t\"symbol\" : \'"+key.getSecond()+"\',\n" +
                        "\t\t\t\"destination\" : \""+q+"\"\n\t\t}");
            });
        });

        var transitionsFormatAll = String.join(",\n", transitionsFormat);
        sb.append("\t");
        sb.append("\"transitions\" : [").append("\n");
        sb.append(transitionsFormatAll).append("\n\t],");

        sb.append("\n\t\"initial\" : \"");
        sb.append(initial);
        sb.append("\",\n");

        sb.append("\t\"final_states\" : [\"");

        var finalFormat = String.join("\", \"", finalStates);
        sb.append(finalFormat).append("\"]\n");
        sb.append("}");

        return sb.toString();
    }
}
