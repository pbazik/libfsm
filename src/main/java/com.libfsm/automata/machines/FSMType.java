package com.libfsm.automata.machines;

public enum FSMType {
    DFA, NFA
}
